# :mortar_board: **ECS Quentin C.**
:link:&nbsp; = fiches perso

## :earth_asia: **Histoire Géographie Géopolitique du Monde Contemporain**
[![Alt Description](https://raw.githubusercontent.com/Tomcattt/Tomcattt.github.io/master/vignette_1.png)](https://realtimeboard.com/app/board/o9J_k0YICf0=/)
:link:&nbsp;[__MappeMonde__](https://realtimeboard.com/app/board/o9J_k0YICf0=/)   *Quelques notes et fonds de cartes, chargement parfois un peu long, bug avec safari*

[![Alt Description](https://raw.githubusercontent.com/Tomcattt/Tomcattt.github.io/master/vignette_4.png)](https://quizlet.com/_4zman7)
:link:&nbsp;[__Quizlet Statistiques__](https://quizlet.com/_4zman7) *Statistiques relevées, mdp : tomcat*

### Timelines
[![Alt Description](https://raw.githubusercontent.com/Tomcattt/Tomcattt.github.io/master/vignette_2.png)](https://tomcattt.github.io/petrole_et_energie)
*Notes prises selon le cours et d'après quelques acticles, évènements à retenir, affichage graphique*
#### :link:&nbsp; Chapitres Continents
* [Asie](https://tomcattt.github.io/asie)
* [Amérique Latine](https://tomcattt.github.io/al)
* [Europe](https://tomcattt.github.io/europe)
* [États-Unis D'Amériques](https://tomcattt.github.io/eua)
* [Moyen Orient](https://tomcattt.github.io/mo)
* [Afrique](https://tomcattt.github.io/afrique)


#### :link:&nbsp; Chapitres complémentaires
* [Pays Emergents](https://tomcattt.github.io/emergents)
* [Villes](https://tomcattt.github.io/les_villes)
* [Migrations](https://tomcattt.github.io/migration)
* [Matières premières](https://tomcattt.github.io/mpremieres)
* [Pétrole et énergies](https://tomcattt.github.io/petrole_et_energie)
* [Commerce International](https://tomcattt.github.io/com)
* [Mers et Espaces Maritimes](https://tomcattt.github.io/mers)
* [France](https://tomcattt.github.io/france)
* [L'Eau](https://tomcattt.github.io/eau)
* [Agriculture et Matières Premières Agricoles](https://tomcattt.github.io/agriculture)
* [Environnement et développement durable](https://tomcattt.github.io/devdurable)
* [Dynamiques Démographiques](https://tomcattt.github.io/demographie)
* [Transports](https://tomcattt.github.io/trs)
* [Tiers-Monde](https://tomcattt.github.io/tm)
* [Mondialisation, inégalités et états](https://tomcattt.github.io/mond_et_ine)
* [Frontières et Mondialisation](https://tomcattt.github.io/frontieres)

:link:&nbsp;[__Timeline pour impression__](https://github.com/Tomcattt/Tomcattt.github.io/tree/master/print)   *Timeline précédentes optimisées pour l'impression*

#### **Ressources**
*dont je ne possède pas les droits*
* [Histoire de comprendre](https://www.dailymotion.com/playlist/x1sh0p)
* [DiploWeb](https://www.diploweb.com)
* [Statistiques de l'OCDE](https://data.oecd.org/fr/)
* [Site de Guillaume Pitron](http://www.guillaumepitron.com/)
* [Mind the Map](http://mindthemap.fr)
* [Groupe d'études géopolitiques](http://gegeurope.org) *think tank de l'Ens*
* [La lettre du Lundi](https://lldl.eu/fr/home/) *newsletter de ce think tank*


## :us: **Anglais**
:link:&nbsp;[__Vocabulaire HIR__](https://tomcattt.github.io/main2.pdf) *Vocabulaire relevé à partir du dernier [HIR](http://hir.harvard.edu) __Spring 2018__*

*lecture régulière*
* [HIR](http://hir.harvard.edu)
* [MTR](https://www.technologyreview.com/the-download/)
* [The Economist](https://www.economist.com)
* [The Economist Films](https://films.economist.com)
* [The Daily Show](https://www.youtube.com/watch?v=LSg_qJYoa4s&list=PLLHHcQWkLnpolBvTPKsnSUiLiWAucX48o) *un must see*


## :heavy_dollar_sign: **Économie**
:link:&nbsp;[__Histoire des idées économiques__](https://tomcattt.github.io/main.pdf)   *Notes prises en suivant le MOOC de [l'école de la liberté](http://www.ecoleliberte.fr/ressource/histoire-des-idees-economiques/)*
* [Libre](http://www.libres.org)
* [Institut Coppet](https://www.institutcoppet.org)

## :performing_arts: **Culture Générale**
:link:&nbsp;[__Timeline Philosophie__](https://tomcattt.github.io/philosophie) *bref récapitulatif non exhaustif de [ce superbe ouvrage](https://www.editions-ellipses.fr/product_info.php?products_id=6285)*

#### Le Corps   
*Notes d'après le cours et suite au fichage de quelques ouvrages portant sur le thème du corps (librairie Vrin)*

:link:&nbsp; [Chapitre 3 : Mécanisme et Théorie des passions](https://tomcattt.github.io/MECANISME_ET_THEORIE_DES_PASSIONS.pdf)

:link:&nbsp; [Chapitre 4 : Existentialisme et phénoménologie](http://tomcattt.github.io/CHAPITRE_4_EXISTENTIALISME_ET_PHENOMENOLOGIE.pdf)

:link:&nbsp; [Chapitre 5 : Le Corps Politique](https://tomcattt.github.io/LE_CORPS_POLITIQUE.pdf)

:link:&nbsp; [Art du portrait et représentation du pouvoir](https://tomcattt.github.io/Art_du_Portrait_et_Representation_du_Pouvoir.pdf)

:link:&nbsp; [Statut du Corps dans les différentes religions & civilisations](https://tomcattt.github.io/ILLUSTRATIONS_INTRODUCTION.pdf)

:link:&nbsp; [Chapitre 3 : Littérature et Poésie du Corps](https://tomcattt.github.io/ILLUSTRATIONS_CHAPITRE_3_LITTERATURE_ET_POESIE_DU_CORPS.pdf)

:link:&nbsp; [Chapitre 4 : Esthétique et Morphologie du Corps](https://tomcattt.github.io/ILLUSTRATIONS_CHAPITRE_4_ESTHETIQUE_ET_MORPHOLOGIE_DU_CORPS.pdf)

:link:&nbsp; [Le corps à l’épreuve du numérique](https://tomcattt.github.io/SENEVE_A_LEPREUVE_DU_NUMERIQUE.pdf)

#### **Ressources**   
*dont je ne possède pas les droits*
* [Cours Corps](http://www.ac-grenoble.fr/PhiloSophie/old2/articles.php?lng=fr&pg=4018)
* [Louis Marin](http://www.louismarin.fr/spip.php?article25)
* [Érudit](https://www.erudit.org/fr/)
* [Fiches Livres](http://appli6.hec.fr/amo/)
* [Les Ernest](http://www.les-ernest.fr)

## :chart_with_upwards_trend: **Mathématiques**
* [ECS2 2017-18 Fauriel](http://ecs2-fauriel.fr/public/Cours/Integrale.pdf)
* [Cours](http://ecs2-fauriel.fr//index.php?pages/Cours)
* [JF Cossutta](http://jfcossutta.lycee-berthelot.fr)
* [Alain Troesch](http://alain.troesch.free.fr/index2012.html)
* [Lycée Poincaré](http://ecs2poincare.free.fr/index_cours_exos.html)
* [Lycée La Bruyère](http://www.rblld.fr/ecs2lb/index.php/2017-2018/td)
* [UPS](https://concours-maths-cpge.fr)
* [Compléments Maths ECS](https://fr.wikiversity.org/wiki/Complément_de_mathématiques_pour_prépa_HEC)
* [Cahier de Vacances PSI* rentrée 2018 Saint Louis](https://tomcattt.github.io/CahierVacances.pdf)

#### Chapitres
- [x] Chapitre 1 : Rappels sur les suites et les séries
- [x] Chapitre 2 : Variables aléatoires discrètes. Espérance conditionnelle
- [x] Chapitre 3 : Rappels et compléments d'algèbre linéaire
- [x] Chapitre 4 : Intégrales impropres
- [x] Chapitre 5 : Matrices
- [x] Chapitre 6 : Couples de variables aléatoires. Covariance
- [x] Chapitre 7 : Valeurs propres, vecteurs propres
- [x] Chapitre 8 : Variables aléatoires à densité
- [x] Chapitre 9 : Produits scalaires, espaces euclidiens
- [x] Chapitre 10 : Diagonalisation
- [x] Chapitre 11 : Vecteurs aléatoires
- [x] Chapitre 12 : Supplémentaire orthogonal
- [x] Chapitre 13 : Fonctions de plusieurs variables
- [x] Chapitre 14 : Convergence des variables
- [x] Chapitre 15 : Endomorphismes et matrices symétriques
- [x] Chapitre 16 : Estimation ponctuelle
- [x] Chapitre 17 : Topologie de Rn. Calcul différentiel d’ordre 2
- [X] Chapitre 18 : Estimation par intervalle de confiance
- [X] Chapitre 19 : Optimisation sous contraintes

## :computer: Informatique
- [x] TP 1 : Manipulation de matrices
- [x] TP 2 : Simulation de lois discrètes
- [X] TP 3 : Statistiques à une variable
- [X] TP 4 : Chaînes de Markov I
- [X] TP 5 : Chaînes de Markov II
- [X] TP 6 : Statistiques bivariées
- [X] TP 7 : Simulation de lois à densité
- [X] TP 8 : Fonctions de plusieurs variables
- [X] TP 9 : Estimation ponctuelle et par intervalle

## :it: **Italien**
* [Italangue](http://www.italangue.com)
* [Ray Play Radio](https://www.raiplayradio.it)

## :computer: Outils & LaTeX
* [Microtypography (améliorer le texte justifié)](https://www.wikiwand.com/en/Microtypography)

## :blue_book: **Autres**
* [Annales BCE](https://www.iscparis.com/admissions-isc-paris/master-grande-ecole/classes-preparatoires/preparations-aux-ecrits/annales/)

## :musical_note: **Chill**
* [?](https://youtu.be/PPQzctf_wIk)
